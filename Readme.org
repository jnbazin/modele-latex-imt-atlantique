#+TITLE: Mod�le de document LaTeX IMT Atlantique pour la r�daction de rapports de recherche (valid� par la DRI) ou des supports de cours et TP, *.tex, *.sty
#+AUTHOR: Thierry Le Gall
#+AUTHOR: Jean-No�l Bazin
#+email: thierry.legall1@imt-atlantique.fr
#+email: jn.bazin@imt-atlantique.fr
#+LANGUAGE: fr
#+STARTUP: showeverything

INSTITUT MINES-TELECOM, IMT ATlantique

D�partement Mathematical & Electrical Engineering

Technop�le de Brest-Iroise

CS 83818 - 29238 BREST Cedex 3

* Description du mod�le:
#+BEGIN_EXAMPLE
 lab_006
 |-img : < images (*.png, *.jpg), associ�es au corps du document >
 |  |-structure_trame_tx.jpg : exemple de figure au format (*.jpg) int�gr�e (au corps du document)
 |  |-structure_trame_tx.png : exemple de figure au format (*.png) int�gr�e (au corps du document)
 |
 |-src : < fichiers sources (*.tex) et produits de compilation LaTeX (*.pdf, *.aux, etc...)
 |  |-tp_latex_lab_006.tex   : fichier source (*.tex) du mod�le (�ditable et modifiable)
 |
 ||-sty : < fichiers de style, logo et images associ�s au mod�le IMT Atlantique (*.sty) >
 |  |-imta.sty               : fichier de style (*.sty) du mod�le (�ditable et modifiable)
 |  |-imta_logo.pdf          : logo de l'IMT Atlantique (1ere et 4eme de couverture)
 |  |-imta_map.pdf           : carte des partenariats et implantations IMT Atlantique (4eme de couverture)
 |  |-imta_triangles.pdf     : figure de style du service de la com. (1ere de couverture)
#+END_EXAMPLE


* Utilisation du mod�le:
** d�compresser localement l'archive (*.zip)
** ouvrir le fichier tp_latex_lab_006.tex dans un �diteur LaTeX
** sauvegarder le fichier tp_latex_lab_006.tex sous un nouveau nom (*.tex)
    au m�me emplacement dans le sous-r�pertoire "src"
** compiler le fichier enregistr� (Ctrl +F7 dans TeXnicCenter)
** visualiser le *.pdf obtenu (F5 dans dans TeXnicCenter)

N.B. si le document appartient � la cat�gorie des "Rapports de recherche d'IMT Atlantique",
la mention de copyright est affich�e en 4�me de couverure avec le n� ISSN attribu� par la
Biblioth�que Nationale de France. Si le document n'appartient pas � cette cat�gorie, la
mention de copyright ainsi que le n�ISSN sont d�sactivables (cf. fichier *.tex).

* R�solution des probl�mes de compilation:
Le mod�le "tp_latex_lab_006.tex" est valid� en �dition LaTeX to PDF dans les environnements
suivants :

- TeXnicCenter 2.02 (stable) + compilateur MiKTeX 2.9.6888 / Window 7 ou 10 (x64)
- TeXstudio 2.12.10 + compilateur MiKTeX 2.9.6888 / Window 7 ou 10 (x64)
- TeXstudio 2.12.6 + distribution TeXlive / UBUNTU 18.04 LTS

Toutefois, des erreurs de compilation peuvent intervenir � la premi�re utlisation
du mod�le sur une machine dont le compilateur MikTeK (Windows) ou TeXlive (Unix)
ne dispose pas initialement de tous les styles requis (packages manquants).

R�solution des erreurs de compilation (droits d'administrateurs requis) :

** Windows :

- Lancer l'application "MiKTeX Console" en mode administrateur
- Overview -> Check for Updates (mise � jour du conpilateur)
- Packages -> Identifier les packages manquants (message d'erreur de compilation) et les installer

** UBUNTU 18.04 ou 20.04 LTS - Ouvrir un Terminal (crtl+alt+t)
#+BEGIN_SRC bash
sudo apt-get update                         #(verifie les mises � jour disponibles pour l'OS)
sudo apt-get upgrade                        #(installe les mises � jour disponibles pour l'OS)
sudo apt-cache search texlive               #(identifie les packages disponibles de texlive)
sudo apt-get install texlive-lang-french    #(r�solution de : 'newtxtext.sty' not found)
sudo apt-get install texlive-fonts-extra    #(r�solution de : 'babel.sty' Package label error)
#+END_SRC
